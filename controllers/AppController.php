<?php
/**
 * Created by PhpStorm.
 * User: cysiu
 * Date: 23.10.2018
 * Time: 08:27
 */

namespace controllers;


class AppController
{
    private $request = null;

    public function __construct()
    {
        $this->request = strtoupper($_SERVER['REQUEST_METHOD']);

    }

    public function isGet()
    {
        return $this->request == 'GET';
    }

    public function isPost()
    {
        return $this->request == 'POST';
    }

    public function render(string $filename = null, array $variables = [])
    {
        $class = get_class($this);
        $class = (substr($class, strrpos($class, '\\') + 1));
        $view = $filename ? dirname(__DIR__).'\\view\\'.$class.'\\'.$filename.'.php' : '';

        //echo $view;

        $output = "There isn't such file to open";

        if(file_exists($view)){
            extract($variables);

            ob_start();
            include($view);
            $output = ob_get_clean();
        }

        print($output);
    }

}
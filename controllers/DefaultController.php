<?php
/**
 * Created by PhpStorm.
 * User: cysiu
 * Date: 23.10.2018
 * Time: 08:27
 */

namespace controllers;

require_once("AppController.php");
require_once("UploadController.php");


class DefaultController extends AppController
{
    public function __construct()
    {
        parent::__construct();

    }

    public function login()
    {
        $this->render("login");
    }

    public function index()
    {
        $name = "Czesław";
        $this->render("index", ['name' => $name]);
    }
}
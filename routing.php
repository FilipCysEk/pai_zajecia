<?php

require_once ("controllers/DefaultController.php");

class Routing
{
    public $routes = array();

    public function __construct()
    {
        //$class = new controllers\DefaultController();
        $this->routes = [
            'index' => [
                'controller' => 'DefaultController',
                'action' => 'index'
            ],
            [
                'controller' => 'DefaultController',
                'action' => 'login'
            ],
            'upload' => [
                'controller' => 'UploadController',
                'action' => 'upload'
            ]
        ];
    }

    public function run()
    {
        $page = isset($_GET['page']) && isset($this->routes[$_GET['page']]) ? $_GET['page'] : 'index';

        if($this->routes[$page]){
            $class = 'controllers\\'.$this->routes[$page]['controller'];
            $action = $this->routes[$page]['action'];

            $object = new $class;
            $object->$action();
        }
    }
}